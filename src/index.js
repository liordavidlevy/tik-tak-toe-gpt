const gameBoard = document.querySelector(".game-board");
const cells = document.querySelectorAll(".game-board td");
const resetButton = document.querySelector("#reset-btn");
const languageButton = document.querySelector("#language-btn");
const playerName = document.querySelector("#player-name");

const players = ["X", "O"];
let currentPlayer = players[0];
let board = ["", "", "", "", "", "", "", "", ""];
let isGameOver = false;
let currentLang = "en";
const language = {
    en: {
      player: "Player",
      win: " wins!",
      tie: "It's a tie!",
      reset: "Reset",
      changeLanguage: "Change to Hebrew"
    },
    he: {
      player: "שחקן",
      win: " ניצח!",
      tie: "המשחק הסתיים בתיקו!",
      reset: "איפוס",
      changeLanguage: "החלף לאנגלית"
    }
  };

const checkWin = () => {
  const winConditions = [
    // Rows
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    // Columns
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    // Diagonals
    [0, 4, 8],
    [2, 4, 6],
  ];

  for (let condition of winConditions) {
    const [a, b, c] = condition;
    if (board[a] === board[b] && board[b] === board[c] && board[a] !== "") {
      endGame(board[a] + language.win);
      return true;
    }
  }

  return false;
};

const checkTie = () => {
  if (board.every((cell) => cell !== "")) {
    endGame(language.tie);
    return true;
  }

  return false;
};

const endGame = (message) => {
  isGameOver = true;
  alert(message);
};

const handleClick = (e) => {
  const cell = e.target;
  const row = cell.getAttribute('data-row');
  const col = cell.getAttribute('data-col');
  const index = Number(row) * 3 + Number(col);
  if (board[index] !== "" || isGameOver) return;
  board[index] = currentPlayer;
  cell.textContent = currentPlayer;
  if (checkWin() || checkTie()) return;
  currentPlayer = currentPlayer === players[0] ? players[1] : players[0];
  playerName.textContent = `${language[currentLang].player}: ${currentPlayer}`;
};

const resetBoard = () => {
  cells.forEach((cell) => {
    cell.textContent = "";
  });
  board = ["", "", "", "", "", "", "", "", ""];
  isGameOver = false;
  playerName.textContent = `${language[currentLang].player}: ${currentPlayer}`;
};

const toggleLanguage = () => {
    currentLang = currentLang === "en" ? "he" : "en";

    // Update UI text
    playerName.textContent = `${language[currentLang].player}: ${currentPlayer}`;
    resetButton.textContent = language[currentLang].reset;
    languageButton.textContent = language[currentLang].changeLanguage;

    // Change direction of text if switching to Hebrew
    const body = document.querySelector("body");
    body.style.direction = currentLang === "he" ? "rtl" : "ltr";
};


gameBoard.addEventListener("click", handleClick);
resetButton.addEventListener("click", resetBoard);
languageButton.addEventListener("click", toggleLanguage);
